package paczka;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Andrzej
 *
 */
public class Router {
	
	public ArrayList<Interfejs> zbiorInterfejsowRoutera=null;
	
	public static String MASKA_PODSIECI="255.255.255.0";
	
	public String daneProgramisty="Andrzej Janosz (331 IZZ)";
	
	public String wersjaOprogramowania="1";
	
	public Polecenie polecenie;
	
	public String identyfikatorPolecenia;
	
	public String dalszaCzescPolecenia;
	
	
	
	
	public Router() {
		zbiorInterfejsowRoutera=new ArrayList<Interfejs>();
		
		
	}
	
	/**
	 * @param ipPortu ip przypisywane nowemu interfejsowi
	 * @param rodzajDodawanegoInterfejsu enum RodzajInterfejsu
	 * @param nazwaInterfejsu enum NazwaInterfejsu
	 */
	public void dodajInterfejs(String ipPortu, RodzajInterfejsu rodzajDodawanegoInterfejsu, NazwaInterfejsu nazwaInterfejsu) {
		
		this.zbiorInterfejsowRoutera.add(new Interfejs(ipPortu, MASKA_PODSIECI, rodzajDodawanegoInterfejsu, nazwaInterfejsu));
		System.out.println("Adres portu " + nazwaInterfejsu + " ustawiony na " + ipPortu + "(maska: " + MASKA_PODSIECI + ")");
		
	}
	
	/** 
	 * wyswietla informacje o programiscie i wersji oprogramowania
	 */
	public void wyswietlHelpa() {
		System.out.println(daneProgramisty + " ; " + "wersja oprogramowania: " +  wersjaOprogramowania);
		
	}
	
	/**
	 * @param polecenie 
	 */
	public void parsujPolecenie(String polecenie) {
		
		String[] tablicaPomocnicza;
		tablicaPomocnicza=polecenie.split(" ");
		identyfikatorPolecenia=tablicaPomocnicza[0];
		try {
			this.polecenie=Polecenie.valueOf(identyfikatorPolecenia);
		} catch (IllegalArgumentException e) {
			System.err.println("Podales nieprawidlowe polecenie");
			System.err.println("Sprobuj jeszcze raz");
			
			return;
			}
		if(tablicaPomocnicza.length>1) {
			dalszaCzescPolecenia=polecenie.split(" ")[1];
		}
		
		switch(this.polecenie) {
		case help:
			this.wyswietlHelpa();
			break;
		case ipA:
			this.dodajInterfejs(dalszaCzescPolecenia, RodzajInterfejsu.WEJSCIOWY, NazwaInterfejsu.A);
			break;
		case ipB1:
			this.dodajInterfejs(dalszaCzescPolecenia, RodzajInterfejsu.WYJSCIOWY, NazwaInterfejsu.B1);
			break;
		case ipB2:
			this.dodajInterfejs(dalszaCzescPolecenia, RodzajInterfejsu.WYJSCIOWY, NazwaInterfejsu.B2);
			break;
		case ipC:
			this.dodajInterfejs(dalszaCzescPolecenia, RodzajInterfejsu.KONSOLOWY, NazwaInterfejsu.C);
			break;
		case show:
			
			if(zbiorInterfejsowRoutera.size()>0) {
				for(Interfejs interfejs: zbiorInterfejsowRoutera) {
					interfejs.wyswietlDaneInterfejsu();
				}
			}
			else {
				System.out.println("Nie ma skonfigurowanych zadnych interfejsow");
			}
			break;
		case simA:
			this.symulujPakiet(dalszaCzescPolecenia);
			
			break;
			
		case shutdown:
			
			System.exit(0);
		default:
			System.out.println("Polecenie nierozpoznane. Sprobuj jeszcze jeszcze raz");
			break;
		
		  
		}
		
	}
	
	public void symulujPakiet(String adresDocelowyPakietu) {
		String[] identyfikatorSzukanegoInterfejsu;
		identyfikatorSzukanegoInterfejsu=adresDocelowyPakietu.split("\\.");
		String wynikMetody=new String();
		
		
		
		for(Interfejs interfejs : zbiorInterfejsowRoutera) {
			
			if(interfejs.adresInterfejsu.adresIp.toString().substring(1).split("\\.")[0].equalsIgnoreCase(identyfikatorSzukanegoInterfejsu[0])) {
					if(interfejs.adresInterfejsu.adresIp.toString().substring(1).split("\\.")[1].equalsIgnoreCase(identyfikatorSzukanegoInterfejsu[1])) {
						if(interfejs.adresInterfejsu.adresIp.toString().substring(1).split("\\.")[2].equalsIgnoreCase(identyfikatorSzukanegoInterfejsu[2])) {
							if(interfejs.nazwaInterfejsu==NazwaInterfejsu.A) {
								wynikMetody=("ATAK NA PORT " + interfejs.nazwaInterfejsu.toString() + "- Pakiet " + adresDocelowyPakietu);
								break;
							}
							else {
								wynikMetody=("Pakiet " + adresDocelowyPakietu + " zostal przekazany na port " + interfejs.nazwaInterfejsu + " (" + interfejs.adresInterfejsu.adresIp );
								break;
							}
						}
						else {
							wynikMetody=("Pakiet " + adresDocelowyPakietu + " ODRZUCONY");
							
						
						}
					
					}
					else {
					wynikMetody=("Pakiet " + adresDocelowyPakietu + " ODRZUCONY");
					
					}
				
			}
			else {
				wynikMetody=("Pakiet " + adresDocelowyPakietu + " ODRZUCONY");
				
			}
		}
		
		System.out.println(wynikMetody);
		
	}
	
	
	

}
