package paczka;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author Andrzej
 *
 */
public class Adres {
	
	public InetAddress adresIp;
	public InetAddress maskaPodsieci;
	
	/**
	 * @param adresIp
	 * @param maskaPodsieci
	 */
	public Adres(String adresIp, String maskaPodsieci ) {
		try {
			this.adresIp=InetAddress.getByName(adresIp);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			System.err.println("Niepoprawny adres IP.");
		}
		try {
			this.maskaPodsieci=InetAddress.getByName(maskaPodsieci);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			System.err.println("Niepoprawny adres maski podsieci.");
		}
		
	}

}
