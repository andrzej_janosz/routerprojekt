package paczka;

/**
 * @author Andrzej
 *
 */
public class Interfejs {
	
	public Adres adresInterfejsu;
	public RodzajInterfejsu rodzajInterfejsu;
	public NazwaInterfejsu nazwaInterfejsu;
	
	/**
	 * @param adresIpInterfejsu
	 * @param maskaPodsieciInterfejsu
	 * @param rodzajInterfejsu
	 * @param nazwaInterfejsu
	 */
	public Interfejs(String adresIpInterfejsu, String maskaPodsieciInterfejsu, RodzajInterfejsu rodzajInterfejsu, NazwaInterfejsu nazwaInterfejsu) {
		this.adresInterfejsu=new Adres(adresIpInterfejsu, maskaPodsieciInterfejsu);
		this.rodzajInterfejsu=rodzajInterfejsu;
		this.nazwaInterfejsu=nazwaInterfejsu;
	}
	
	/**
	 * 
	 */
	public void wyswietlDaneInterfejsu() {
		System.out.println("Interfejs " + this.nazwaInterfejsu + ": IP: " + this.adresInterfejsu.adresIp + " (maska:" + adresInterfejsu.maskaPodsieci + ")");
	}

}
